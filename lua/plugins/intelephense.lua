return {
  {
    "neovim/nvim-lspconfig",
    opts = {
      servers = {
        intelephense = {
          settings = {
            intelephense = {
              files = {
                associations = { "*.php", "*.inc" },
              },
            },
          },
        },
      },
    },
  },
}
