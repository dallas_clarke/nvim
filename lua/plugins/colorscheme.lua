
return {
  { "nyoom-engineering/oxocarbon.nvim" },
  { "catppuccin/nvim", name = "catppuccin", priority = 1000 },
  { "EdenEast/nightfox.nvim" },
  { "rose-pine/neovim", name = "rose-pine" },
  { "olimorris/onedarkpro.nvim", priority = 1000 },

  {
    "LazyVim/LazyVim",
    opts = {
      colorscheme = "onedark_vivid"
    },
  },
}
