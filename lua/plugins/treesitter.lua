return {
  "nvim-treesitter/nvim-treesitter",
  opts = {
    indent = {
      enable = true,
    },
    autotag = {
      enable = true
    },
  },
}
